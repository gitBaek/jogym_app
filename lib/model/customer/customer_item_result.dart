import 'package:jo_gym_app/model/customer/customer_item.dart';
import 'package:jo_gym_app/model/member/login_response.dart';

// "currentPage": 0,
//  "totalItemCount": 0,
//  "totalPage": 0`
// "list": [
//         {
//             "dateCreate": "string",
//             "id": 0,
//             "name": "string",
//             "phoneNumber": "string"
//         }
//  ],

class CustomerItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<CustomerItem>? list;


  CustomerItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory CustomerItemResult.fromJson(Map<String, dynamic> json) {
    return CustomerItemResult(
        json['currentPage'],
        json['totalItemCount'],
        json['totalPage'],
        list: json['list'] != null ? (json['list'] as List).map((e) => CustomerItem.fromJson(e)).toList() : [],
    );
  }
}
