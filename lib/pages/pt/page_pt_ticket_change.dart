import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/middleware/middleware_login_check.dart';
import 'package:jo_gym_app/model/member/login_request.dart';
import 'package:jo_gym_app/pages/page_login.dart';
import 'package:jo_gym_app/repository/member/repo_member.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

import 'page_pt_ticket_select_trainer_list.dart';

class PagePtTicketChange extends StatefulWidget {
  const PagePtTicketChange(
      {super.key,
      required this.ptTicketId,
      required this.maxCount,
      required this.unitPrice});

  final int ptTicketId;
  final double unitPrice;
  final int maxCount;

  @override
  State<PagePtTicketChange> createState() => _PagePtTicketChangeState();
}

class _PagePtTicketChangeState extends State<PagePtTicketChange> {
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textControllerOfMaxMonth =
      TextEditingController();
  final TextEditingController _textControllerOfUnitPrice =
      TextEditingController();

  bool _isFixEnable = false;

  @override
  void initState() {
    super.initState();

    _textControllerOfMaxMonth.text = widget.maxCount.toString();
    _textControllerOfUnitPrice.text = maskWonFormatter.format(widget.unitPrice);


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'PT권 정보 수정'),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: bodyPaddingLeftRight,
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'maxCount',
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('총 횟수'),
                          maxLength: 20,
                          controller: _textControllerOfMaxMonth,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(1,
                                errorText: formErrorMinLength(1)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          enabled: _isFixEnable,
                          controller: _textControllerOfUnitPrice,
                          decoration: StyleFormDecoration()
                              .getInputDecoration('요금 / 횟수'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: !_isFixEnable
                            ? ComponentTextBtn(
                                '수정',
                                () {
                                  setState(() {
                                    _isFixEnable = !_isFixEnable;
                                  });
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              )
                            : ComponentTextBtn(
                                '저장',
                                () {
                                  // "trainerId"
                                  //  "maxMonth": 0,
                                  // "ticketName": "string",
                                  // "unitPrice": 0

                                  if (_formKey.currentState!
                                      .saveAndValidate()) {
                                    print(
                                        "maxMonth : ${_formKey.currentState!.fields['maxCount']!.value}");
                                    print(
                                        "unitPrice : ${_formKey.currentState!.fields['unitPrice']!.value}");
                                  }
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '삭제',
                              () {
                            print("삭제");
                          },
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
