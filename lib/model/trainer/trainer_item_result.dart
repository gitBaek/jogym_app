import 'package:jo_gym_app/model/customer/customer_item.dart';
import 'package:jo_gym_app/model/member/login_response.dart';
import 'package:jo_gym_app/model/trainer/trainer_item.dart';

// {
// "code": 0,
// "currentPage": 0,
// "isSuccess": true,
// "list": [
// {
//     "dateCreate": "string",
//     "id": 0,
//     "name": "string",
//     "phoneNumber": "string"
// }
// ],
// "msg": "string",
// "totalItemCount": 0,
// "totalPage": 0
// }

class TrainerItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<TrainerItem>? list;


  TrainerItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory TrainerItemResult.fromJson(Map<String, dynamic> json) {
    return TrainerItemResult(
      json['currentPage'],
      json['totalItemCount'],
      json['totalPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => TrainerItem.fromJson(e)).toList() : [],
    );
  }
}
