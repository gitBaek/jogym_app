class CalculateResponse {
  int dateCreateYear;
  int dateCreateMonth;
  double feesRate;
  double totalPrice;
  double minusPrice;
  double calculatePrice;
  String calculateStatus;

  CalculateResponse(
      this.dateCreateYear,
      this.dateCreateMonth,
      this.feesRate,
      this.totalPrice,
      this.minusPrice,
      this.calculatePrice,
      this.calculateStatus,
      );

  // "dateCreateYear": 2022,
  // "dateCreateMonth": 7,
  // "feesRate": 0.12,
  // "totalPrice": 0,
  // "minusPrice": 0,
  // "calculatePrice": 0,
  // "calculateStatus": "ING"


  factory CalculateResponse.fromJson(Map<String, dynamic> json) {
    return CalculateResponse(
        json['dateCreateYear'],
        json['dateCreateMonth'],
        json['feesRate'],
        json['totalPrice'],
        json['minusPrice'],
        json['calculatePrice'],
        json['calculateStatus']);
  }




  // private Integer dateCreateYear;
  // private Integer dateCreateMonth;
  // private Double feesRate;
  // private Double totalPrice;
  // private Double minusPrice;
  // private Double calculatePrice;
  // private CalculateStatus calculateStatus;
}