import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/buy/buy_pt_ticket_request.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/customer/customer_create_request.dart';
import 'package:jo_gym_app/model/customer/customer_item_result.dart';
import 'package:jo_gym_app/model/customer/customer_response_result.dart';
import 'package:jo_gym_app/model/customer/customer_update_request.dart';

class RepoBuyPtTicket {
  Future<CommonResult> doBuyPtTicket(BuyPtTicketRequest request) async {
    // const String baseUrl = '$apiUri/customer/data';
     const String baseUrl = 'http://localhost:8082/v1/pt-ticket-buy/purchase';


    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

}
