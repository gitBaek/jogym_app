import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_no_contents.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/card/component_calculate_card.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/model/calculate/calculate_item.dart';
import 'package:jo_gym_app/pages/customer/page_customer_detail.dart';
import 'package:jo_gym_app/repository/calculate/repo_calculate.dart';

import 'page_calculate_detail.dart';

class PageCalculate extends StatefulWidget {
  const PageCalculate({super.key});

  @override
  State<PageCalculate> createState() => _PageCalculateState();
}

class _PageCalculateState extends State<PageCalculate> {
  final _scrollController = ScrollController();

  List<CalculateItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoCalculate()
          .getList(page: _currentPage)
          .then((res) => {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [
            ..._list,
            ...?res.list
          ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      })
          .catchError((err) => {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose(); // dispose = StatefulWidget이 클래스가 종료될 때 호출
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarPopup(title: '정산 리스트'),
        body: Column(
          children: [
            Container(
                alignment: Alignment.centerRight,
                padding: bodyPaddingLeftRightAndVerticalHalf,
                child: SizedBox(
                  width: 100,
                  child: ComponentTextBtn(
                    '불러오기',
                        () {
                          _loadItems(reFresh: true);
                    },
                    bgColor: Colors.blue,
                    borderColor: Colors.white,
                  ),
                )
            ), //검색 // 신규회원등록
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                padding: bodyPaddingLeftRight,
                child: _buildBody(),
              ),
            ),
          ],
        )
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentCalculateCard(
                  number: _list[index].id,
                  dateCreateYear: _list[index].dateCreateYear,
                  dateCreateMonth: _list[index].dateCreateMonth,
                  calculateStatus: _list[index].calculateStatus,
                  voidCallback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                PageCalculateDetail(calculateId: _list[index].id)));
                  })
          ),
        ],
      );
    }
    else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents(
            icon: Icons.history,
            msg: '등록된 정산내역이 없습니다.',
          ),
          onTap: () => _loadItems(reFresh: true),
        ),
      );
    }
  }
}

