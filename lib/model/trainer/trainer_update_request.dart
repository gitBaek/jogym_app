class TrainerUpdateRequest {
  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;
  String? careerContent;
  String? memo;

  TrainerUpdateRequest(this.name, this.phoneNumber, this.address, this.gender,
      this.dateBirth, this.careerContent, this.memo);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;
    data['gender'] = gender;
    data['dateBirth'] = dateBirth;
    data['careerContent'] = memo;
    data['memo'] = memo;
    return data;
  }
}

// "address": "string",
// "careerContent": "string",
// "dateBirth": "string",
// "gender": "MALE",
// "memo": "string",
// "name": "string",
// "phoneNumber": "string"
