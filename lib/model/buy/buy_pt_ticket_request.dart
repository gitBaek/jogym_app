class BuyPtTicketRequest {

  int customerId;
  int ptTicketId;


  // "customerId": 1,
  // "ptTicketId": 1

  BuyPtTicketRequest(this.customerId, this.ptTicketId);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['customerId'] = customerId;
    data['ptTicketId'] = ptTicketId;

    return data;
  }
}