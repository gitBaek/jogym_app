import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_pt_ticket.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_change.dart';

import 'page_pt_ticket_create.dart';

class PagePtTicket extends StatefulWidget {
  const PagePtTicket({super.key});

  @override
  State<PagePtTicket> createState() => _PagePtTicketState();
}

class _PagePtTicketState extends State<PagePtTicket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: "PT권 정보 관리",
      ),
      body: Column(children: [
        Container(
          alignment: Alignment.centerRight,
          padding: bodyPaddingLeftRightAndVerticalHalf,
          child: SizedBox(
            width: 100,
            child: ComponentTextBtn(
              '등록',
                  () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            PagePtTicketCreate()));
              },
              bgColor: Colors.blue,
              borderColor: Colors.white,
            ),
          ),
        ), // 등록 버튼
        Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              child: Column(
                children: [
                  ComponentPtTicket(
                    number: 1,
                    ptTicketName: "옆집 로니",
                    maxCount: "50회/원",
                    unitPrice: "1,000,000,000원",
                    profileImg: Image.asset("assets/roni.png"),
                    voidCallback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PagePtTicketChange(ptTicketId: 1,maxCount: 12, unitPrice: 123456789,)));
                    },
                  ),
                ],
              ),
            )),
      ]),
    );
  }
}
