import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_season_ticket.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_change.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_create.dart';

class PageSeasonTicket extends StatefulWidget {
  const PageSeasonTicket({super.key});

  @override
  State<PageSeasonTicket> createState() => _PageSeasonTicketState();
}

class _PageSeasonTicketState extends State<PageSeasonTicket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: "정기권 정보 관리",
      ),
      body: Column(children: [
        Container(
          alignment: Alignment.centerRight,
          padding: bodyPaddingLeftRightAndVerticalHalf,
          child: SizedBox(
            width: 100,
            child: ComponentTextBtn(
              '등록',
              () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            PageSeasonTicketCreate()));
              },
              bgColor: Colors.blue,
              borderColor: Colors.white,
            ),
          ),
        ), // 등록 버튼
        Expanded(
            child: SingleChildScrollView(
          padding: bodyPaddingLeftRight,
          child: Column(
            children: [
              ComponentSeasonTicket(
                  number: 1,
                  seasonTicketName: "6개월 정기권",
                  seasonTicketType: "정기권",
                  month: "6 개월",
                  unitPrice: "100,000 원/개월",
                  totalPrice: 0.toString(),
                  profileImg: Image.asset("assets/ss.png"),
                  voidCallback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                PageSeasonTicketChange(seasonTicketId: 1,)));
                  },),
            ],
          ),
        )),
      ]),
    );
  }
}
