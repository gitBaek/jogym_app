import 'package:jo_gym_app/model/calculate/calculate_response.dart';

class CalculateResponseResult {
  CalculateResponse data;
  bool isSuccess;
  int code;
  String msg;

  CalculateResponseResult(this.data, this.isSuccess, this.code, this.msg);

  factory CalculateResponseResult.fromJson(Map<String, dynamic> json) {
    return CalculateResponseResult(
        CalculateResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}