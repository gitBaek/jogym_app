import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageSeasonTicketChange extends StatefulWidget {
  const PageSeasonTicketChange({super.key, required this.seasonTicketId});

  final int seasonTicketId;

  @override
  State<PageSeasonTicketChange> createState() => _PageSeasonTicketChangeState();
}

// "ticketName": "string",
// "ticketType": "MONTH",
// "maxMonth": 0,
// "unitPrice": 0

class _PageSeasonTicketChangeState extends State<PageSeasonTicketChange> {
  final _formKey = GlobalKey<FormBuilderState>();

  final TextEditingController _textControllerOfTicketName =
      TextEditingController();
  final TextEditingController _textControllerOfMaxMonth =
      TextEditingController();
  final TextEditingController _textControllerOfUnitPrice =
      TextEditingController();
  String _initValueOfTicketType = "MONTH";
  bool _isFixEnable = false;

  @override
  void initState() {
    super.initState();
    _textControllerOfTicketName.text = "정기권명";
    _initValueOfTicketType = "MONTH";
    _textControllerOfMaxMonth.text = 10.toString();
    _textControllerOfUnitPrice.text = maskNumFormatter.format(123456789) + " 원";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "정기권 정보 수정"),
      body: Container(
        child: SingleChildScrollView(
          padding: bodyPaddingLeftRight,
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'ticketName',
                          controller: _textControllerOfTicketName,
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('정기권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderDropdown<String>(
                          name: 'ticketType',
                          initialValue: _initValueOfTicketType,
                          enabled: _isFixEnable,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                          decoration: StyleFormDecoration()
                              .getInputDecoration('정기권 타입'),
                          items: const [
                            DropdownMenuItem(value: "MONTH", child: Text('월')),
                            DropdownMenuItem(value: "DAY", child: Text('일')),
                          ],
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'maxMonth',
                          controller: _textControllerOfMaxMonth,
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('월'),
                          keyboardType: TextInputType.text,
                          inputFormatters: [maskMonthInputFormatter],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.min(1,
                                errorText: formErrorMinNumber(1)),
                            FormBuilderValidators.max(12,
                                errorText: formErrorMaxNumber(12))
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          controller: _textControllerOfUnitPrice,
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('월 요금'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: !_isFixEnable
                            ? ComponentTextBtn(
                                '수정',
                                () {
                                  setState(() {
                                    _isFixEnable = !_isFixEnable;
                                  });
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              )
                            : ComponentTextBtn(
                                '저장',
                                () {
                                  if (_formKey.currentState!
                                      .saveAndValidate()) {
                                    // "ticketName": "string",
                                    // "ticketType": "MONTH",
                                    // "maxMonth": 0,
                                    // "unitPrice": 0
                                    print(
                                        "ticketName : ${_formKey.currentState!.fields['ticketName']!.value}");
                                    print(
                                        "ticketType : ${_formKey.currentState!.fields['ticketType']!.value}");
                                    print(
                                        "maxMonth : ${_formKey.currentState!.fields['maxMonth']!.value}");
                                    print(
                                        "unitPrice : ${_formKey.currentState!.fields['unitPrice']!.value}");
                                    //_setCustomer(request);
                                    setState(() {
                                      _isFixEnable = !_isFixEnable;
                                    });
                                  }
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '삭제',
                          () {
                            print("삭제");
                          },
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
