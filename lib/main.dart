import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/login_check.dart';
import 'package:jo_gym_app/pages/customer/page_customer_buy_history.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/pages/page_login.dart';
import 'package:jo_gym_app/pages/page_test.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket_change.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'JO GYM',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: colorBackGround,
      ),
       home: LoginCheck(),         // 로그인 체크 - 완료
       // home: PageHome(),
      //home: PageTest(),
      // home: PageLogin(),
    );
  }

//백준영 작업  내역
//home: PageMember(), //- 디자인 완료
//home: PageMemberInfoDetail(), // 디자인 완료
//home: PagePtTicketList(), //- 디자인 완료




}
// 폰으로 했을 때 문제있는것.
// PageMemberBuyHistory() - 완료
// PageMemberInfoDetail() --> 버튼 수정 필요 완료
// PagePtTicket() - 완료
// PagePtTicketBuyHistory // 완료
// PagePtTicketList - 완료
// PagePtTicketUseHistory --> 완료
// PageSeasonBuyHistory --> 완료
// PageSeasonTicket
// PageSeasonTicketList
// PageSeasonTicketUseHistory --> 버튼
// PageTrainerMember