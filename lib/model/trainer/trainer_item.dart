class TrainerItem {
  int id;
  String dateCreate;
  String name;
  String phoneNumber;

  // "dateCreate": "string",
  // "id": 0,
  // "name": "string",
  // "phoneNumber": "string"

  TrainerItem(this.id, this.dateCreate, this.name, this.phoneNumber);

  factory TrainerItem.fromJson(Map<String, dynamic> json) {
    return TrainerItem(
        json['id'],
        json['dateCreate'],
        json['name'],
        json['phoneNumber']
    );
  }
}
