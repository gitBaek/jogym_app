import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/buy_history/component_pt_buy_history.dart';
import 'package:jo_gym_app/components/jogym_components/buy_history/components_season_buy_history.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';

class PageCustomerBuyHistory extends StatefulWidget {
  const PageCustomerBuyHistory({super.key, required this.customerId});

  final int customerId;

  @override
  State<PageCustomerBuyHistory> createState() => _PageCustomerBuyHistoryState();
}

// "address": "string",
// "dateBirth": "string",
// "gender": "MALE",
// "name": "string",
// "phoneNumber": "string"

class _PageCustomerBuyHistoryState extends State<PageCustomerBuyHistory> {
  bool _isBtnSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "구매내용"),
      body: Column(
        children: [
          const ComponentMarginVertical(enumSize: EnumSize.mid),
          Container(
            padding: bodyPaddingLeftRightAndVerticalHalf,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (_isBtnSwitch)
                      ? ComponentTextBtn(
                          '정기권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('정기권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (!_isBtnSwitch)
                      ? ComponentTextBtn(
                          'PT권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('PT권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              child: _isBtnSwitch
                  ? _setMockUpData_SeasonTicket()
                  : _setMockUpData_PtTicketHistory(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setMockUpData_SeasonTicket() {
    return Column(
      children: [
        ComponentSeasonBuyHistory(
          number: 1,
          historyName: "12345678901234567890",
          startDate: "startDate",
          endDate: "endDate",
          lastDate: "lastDate",
          leftDays: 10,
          isValid: true,
          profileImg: Image.asset("assets/ss.png"),
          voidCallbackAttendance: () {},
          voidCallbackComplete: () {},
        )
      ],
    );
  }

  Widget _setMockUpData_PtTicketHistory() {
    return Column(
      children: [
        ComponentPtBuyHistory(
          number: 1,
          ptTicketName: "이런전런피티",
          remainSet: 10,
          isValid: false,
          voidCallbackAttendance: () {
            print("출석");
          },
          voidCallbackComplete: () {
            print("완료");
          },
          profileImg: Image.asset("assets/ss.png"),
        )
      ],
    );
  }
}
