import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentSeasonUseHistory extends StatelessWidget {
  const ComponentSeasonUseHistory(
      {super.key,
        required this.number,
        required this.dateUse,
        required this.name,
        required this.voidCallback,});

  final int number;
  final String dateUse;
  final String name;
  //final Image profileImg;
  final VoidCallback voidCallback;

  // "historyId": 0,
  // "historyName": "string",
  // "startDate": "string"
  // "endDate": "string",
  // "lastDate": "string",
  // "leftDays": 0,
  // "buyStatus": "VALID",

  //  일일권명
  //  시작일
  //  종료일
  //  잔여일수
  //  최근방문일

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      _setRowText(subTitle: "No.", subTitleVal: "$number"),
                      _setRowText(subTitle: "사용일시", subTitleVal: dateUse),
                      _setRowText(subTitle: "정기권명", subTitleVal: name),
                      SizedBox(height: 10)
                    ],
                  ),
                ),
                //Container(width: 100, height: 100, child: profileImg),
              ],
            ),
          ],
        ));
  }

  Widget _setRowText({required String subTitle, required String subTitleVal}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 100,
          child: Text(
            subTitle,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Container(
          child: Text(
            subTitleVal,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }
}
