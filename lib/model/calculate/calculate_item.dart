class CalculateItem {
  int id;
  int dateCreateYear;
  int dateCreateMonth;
  String calculateStatus;

  // "calculateStatus": "ING",
  // "dateCreateMonth": 0,
  // "dateCreateYear": 0,
  // "id": 0
  CalculateItem(this.id, this.dateCreateYear, this.dateCreateMonth, this.calculateStatus);

  factory CalculateItem.fromJson(Map<String, dynamic> json) {
    return CalculateItem(
        json['id'],
        json['dateCreateYear'],
        json['dateCreateMonth'],
        json['calculateStatus'],
    );
  }
}