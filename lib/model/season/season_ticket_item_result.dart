import 'season_ticket_item.dart';

// {
//   "code": 0,
//   "currentPage": 0,
//   "isSuccess": true,
//   "list": [
//     {
//        "id": 1,
//       "ticketType": "일일권",
//       "ticketName": "일일권",
//       "maxMonth": 0,
//       "unitPrice": 10000,
//       "totalPrice": 0
//     }
//   ],
//   "msg": "string",
//   "totalItemCount": 0,
//   "totalPage": 0
// }


class SeasonTicketItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<SeasonTicketItem>? list;


  SeasonTicketItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory SeasonTicketItemResult.fromJson(Map<String, dynamic> json) {
    return SeasonTicketItemResult(
      json['currentPage'],
      json['totalItemCount'],
      json['totalPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => SeasonTicketItem.fromJson(e)).toList() : [],
    );
  }
}
