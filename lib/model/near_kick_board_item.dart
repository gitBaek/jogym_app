class NearKickBoardItem {
  String kickBoardStatusName;
  String modelName;
  String priceBasisName;
  num posX;
  num posY;
  num distanceM;

  NearKickBoardItem(this.kickBoardStatusName, this.modelName, this.priceBasisName, this.posX, this.posY, this.distanceM);

  factory NearKickBoardItem.fromJson(Map<String, dynamic> json) {
    return NearKickBoardItem(
      json['kickBoardStatusName'],
      json['modelName'],
      json['priceBasisName'],
      json['posX'],
      json['posY'],
      json['distanceM'],
    );
  }
}