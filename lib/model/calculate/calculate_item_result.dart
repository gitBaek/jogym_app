import 'package:jo_gym_app/model/calculate/calculate_item.dart';

class CalculateItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<CalculateItem>? list;

  CalculateItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory CalculateItemResult.fromJson(Map<String, dynamic> json) {
    return CalculateItemResult(
        json['currentPage'],
        json['totalItemCount'],
        json['totalPage'],
        list: json['list'] != null ? (json['list'] as List).map((e) => CalculateItem.fromJson(e)).toList() : [],
    );
  }
}