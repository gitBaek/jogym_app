import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentCalculateCard extends StatelessWidget {
  const ComponentCalculateCard({super.key,
    required this.number,
    required this.dateCreateYear,
    required this.dateCreateMonth,
    required this.calculateStatus,
    required this.voidCallback,
  });



  final int number;
  final int dateCreateYear;
  final int dateCreateMonth;
  final String calculateStatus;
  final VoidCallback voidCallback;

  // "calculateStatus": "ING", 정산 상태
  // "dateCreateMonth": 0, 등록월
  // "dateCreateYear": 0, 등록년
  // "id": 0 시퀀스

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 300,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 80,
                        child: Text(
                          "No",
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Text(
                        "$number",
                        style: TextStyle(
                          fontSize: fontSizeMid,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 80,
                        child: Text(
                          "등록년",
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Text(
                        "$dateCreateYear",
                        style: TextStyle(
                          fontSize: fontSizeMid,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 80,
                        child: Text(
                          "등록월",
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Text(
                          "$dateCreateMonth",
                        style: TextStyle(
                          fontSize: fontSizeMid,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 80,
                        child: Text(
                          "정산상태",
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Text(
                        calculateStatus,
                        style: TextStyle(
                          fontSize: fontSizeMid,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
