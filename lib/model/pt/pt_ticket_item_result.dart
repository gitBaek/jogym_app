import 'package:jo_gym_app/model/customer/customer_item.dart';
import 'package:jo_gym_app/model/member/login_response.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_item.dart';

// {
//   "isSuccess": true,
//   "code": 0,
//   "msg": "성공하였습니다.",
//   "list": [
//     {
//       "id": 1,
//       "trainerId": 1,
//       "ticketName": "처음PT",
//       "maxCount": 1,
//       "unitPrice": 10000,
//       "totalPrice": 10000
//     }
//   ],
//   "totalItemCount": 3,
//   "totalPage": 1,
//   "currentPage": 1
// }

class PtTicketItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<PtTicketItem>? list;


  PtTicketItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory PtTicketItemResult.fromJson(Map<String, dynamic> json) {
    return PtTicketItemResult(
      json['currentPage'],
      json['totalItemCount'],
      json['totalPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => PtTicketItem.fromJson(e)).toList() : [],
    );
  }
}
