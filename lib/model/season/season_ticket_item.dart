class SeasonTicketItem {
  int id;
  String ticketType;
  String ticketName;
  int maxMonth;
  num unitPrice;
  num totalPrice;

  // "id": 1,
  // "ticketType": "일일권",
  // "ticketName": "일일권",
  // "maxMonth": 0,
  // "unitPrice": 10000,
  // "totalPrice": 0

  SeasonTicketItem(this.id, this.ticketName, this.ticketType, this.maxMonth,
      this.unitPrice, this.totalPrice);

  factory SeasonTicketItem.fromJson(Map<String, dynamic> json) {
    return SeasonTicketItem(
        json['id'],
        json['ticketName'],
        json['ticketType'],
        json['maxMonth'],
        json['unitPrice'],
        json['totalPrice'],
    );
  }
}
