class CalculateUpdateRequest {
  int dateCreateYear;
  int dateCreateMonth;
  double totalPrice;
  double minusPrice;
  double calculatePrice;
  String calculateStatus;

  CalculateUpdateRequest(this.dateCreateYear, this.dateCreateMonth, this.totalPrice, this.minusPrice, this.calculatePrice, this.calculateStatus);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['dateCreateYear'] = dateCreateYear;
    data['dateCreateMonth'] = dateCreateMonth;
    data['totalPrice'] = totalPrice;
    data['minusPrice'] = minusPrice;
    data['calculatePrice'] = calculatePrice;
    data['calculateStatus'] = calculateStatus;
    return data;
  }
}





// Integer dateCreateYear;
// Integer dateCreateMonth;
// Double totalPrice;
// Double minusPrice;
// Double calculatePrice;
// CalculateStatus calculateStatus;