class PtTicketItem {
  int id;
  int trainerId;
  String ticketName;
  int maxCount;
  num unitPrice;
  num totalPrice;

  //      "id": 1,
  //     "trainerId": 1,
  //     "ticketName": "처음PT",
  //     "maxCount": 1,
  //     "unitPrice": 10000,
  //     "totalPrice": 10000

  PtTicketItem(this.id, this.trainerId, this.ticketName, this.maxCount,
      this.unitPrice, this.totalPrice);

  factory PtTicketItem.fromJson(Map<String, dynamic> json) {
    return PtTicketItem(
        json['id'],
        json['trainerId'],
        json['ticketName'],
        json['maxCount'],
        json['unitPrice'],
        json['totalPrice']
    );
  }
}
