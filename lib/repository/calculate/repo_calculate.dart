import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/calculate/calculate_item_result.dart';
import 'package:jo_gym_app/model/calculate/calculate_response_result.dart';
import 'package:jo_gym_app/model/calculate/calculate_update_request.dart';
import 'package:jo_gym_app/model/common_result.dart';

class RepoCalculate {
  Future<CalculateItemResult> getList({required int page}) async { // 정산내역 리스트
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8087/v1/calculate/list/store-member?page={page}';




    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CalculateItemResult.fromJson(response.data);
  }

  Future<CalculateResponseResult> getData({required int calculateId}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8087/v1/calculate/detail/history-id/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', calculateId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CalculateResponseResult.fromJson(response.data);
  }

  Future<CommonResult> putData({required int calculateId, required CalculateUpdateRequest request }) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8087/v1/claculate/{calculateId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(_baseUrl.replaceAll('{calculateId}', calculateId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }
}