class CustomerResponse {
  String dateCreate;
  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;
  String? memo;

  CustomerResponse(this.dateCreate, this.name, this.phoneNumber, this.address,
      this.gender, this.dateBirth, this.memo);

  factory CustomerResponse.fromJson(Map<String, dynamic> json) {
    return CustomerResponse(
        json['dateCreate'],
        json['name'],
        json['phoneNumber'],
        json['address'],
        json['gender'],
        json['dateBirth'],
        json['memo']);
  }
}

// "dateCreate": "2023-11-08 14:47",
// "name": "1234",
// "phoneNumber": "010-0123-1234",
// "address": "asd123",
// "gender": "남자",
// "dateBirth": "2020-01-01",
// "memo": null
