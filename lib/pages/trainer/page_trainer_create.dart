import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/customer/customer_create_request.dart';
import 'package:jo_gym_app/model/trainer/trainer_create_request.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/repository/customer/repo_customer.dart';
import 'package:jo_gym_app/repository/trainer/repo_trainer.dart';
import 'package:jo_gym_app/styles/style_form_decoration_full_going.dart';

class PageTrainerCreate extends StatefulWidget {
  const PageTrainerCreate({super.key});

  @override
  State<PageTrainerCreate> createState() => _PageTrainerCreateState();
}

// "name": "string",
// "phoneNumber": "string"
// "address": "string",
// "gender": "MALE",
// "dateBirth": "string",
// "careerContent": "string",
// "memo": "string",

class _PageTrainerCreateState extends State<PageTrainerCreate> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setTrainer(TrainerCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoTrainer().doCreate(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '직원등록 완료',
        subTitle: '직원등록이 완료되었습니다.',
      ).call();
      Navigator.of(context).pop();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '직원등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '트레이너 등록'),
      body: SingleChildScrollView(
        padding: bodyPaddingLeftRight,
        child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                FormBuilderTextField(
                  name: 'name',
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('직원명'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderTextField(
                  name: 'phoneNumber',
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('연락처'),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    maskPhoneNumberInputFormatter
                    //13자리만 입력받도록 하이픈 2개+숫자 11개
                  ],
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(13,
                        errorText: formErrorMinLength(13)),
                    FormBuilderValidators.maxLength(13,
                        errorText: formErrorMaxLength(13)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderTextField(
                  name: 'address',
                  decoration:
                  StyleFormDecorationOfInputText().getInputDecoration('주소'),
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(100,
                        errorText: formErrorMaxLength(100)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderDropdown<String>(
                  name: 'gender',
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '성별',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                  items: const [
                    DropdownMenuItem(value: "MALE", child: Text('남성')),
                    DropdownMenuItem(value: "FEMALE", child: Text('여성')),
                  ],
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.small,
                ),
                FormBuilderDateTimePicker(
                  name: 'dateBirth',
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                  inputType: InputType.date,
                  firstDate: DateTime(1970),
                  lastDate: DateTime(2030),
                  format: DateFormat('yyyy-MM-dd'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '생년월일',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderTextField(
                  name: 'careerContent',
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                  StyleFormDecorationOfInputText().getInputDecoration('수상내역'),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderTextField(
                  name: 'memo',
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                  StyleFormDecorationOfInputText().getInputDecoration('비고'),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                    width: MediaQuery.sizeOf(context).width,
                    child: ComponentTextBtn('완료', () {
                      if (_formKey.currentState!.saveAndValidate()) {
                        TrainerCreateRequest request = TrainerCreateRequest(
                            _formKey.currentState!.fields['name']!.value,
                            _formKey.currentState!.fields['phoneNumber']!.value,
                            _formKey.currentState!.fields['address']!.value,
                            _formKey.currentState!.fields['gender']!.value,
                            DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['dateBirth']!.value),
                            _formKey.currentState!.fields['careerContent']!.value,
                            _formKey.currentState!.fields['memo']?.value
                        );

                        print("name : ${request.name}");
                        print("phoneNumber : ${request.phoneNumber}");
                        print("address : ${request.address}");
                        print("gender : ${request.gender}");
                        print("dateBirth : ${request.dateBirth}");
                        print("careerContent : ${request.careerContent}");
                        print("memo : ${request.memo}");

                        _setTrainer(request);
                      }
                    })),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
              ],
            )),
      ),
    );
  }
}
