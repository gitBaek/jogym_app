import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/season_ticket_create_request.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/repository/season/repo_season_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageSeasonTicketCreate extends StatefulWidget {
  const PageSeasonTicketCreate({super.key});

  @override
  State<PageSeasonTicketCreate> createState() => _PageSeasonTicketCreateState();
}

class _PageSeasonTicketCreateState extends State<PageSeasonTicketCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  Future<void> _setTicketSeason(SeasonTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoSeasonTicket().doCreate(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '정기권 등록 완료',
        subTitle: '정기권 등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageHome()),
          (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '정기권 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '정기권 정보 등록'),
      body: Container(
        child: SingleChildScrollView(
          padding: bodyPaddingLeftRight,
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'ticketName',
                          decoration:
                              StyleFormDecoration().getInputDecoration('정기권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderDropdown<String>(
                          name: 'ticketType',
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                          decoration: StyleFormDecoration()
                              .getInputDecoration('정기권 타입'),
                          items: const [
                            DropdownMenuItem(value: "MONTH", child: Text('월')),
                            DropdownMenuItem(value: "DAY", child: Text('일')),
                          ],
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'maxMonth',
                          decoration:
                              StyleFormDecoration().getInputDecoration('월'),
                          keyboardType: TextInputType.text,
                          inputFormatters: [maskMonthInputFormatter],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.min(1,
                                errorText: formErrorMinNumber(1)),
                            FormBuilderValidators.max(12,
                                errorText: formErrorMaxNumber(12))
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          decoration:
                              StyleFormDecoration().getInputDecoration('월 요금'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '저장',
                          () {
                            if (_formKey.currentState!.saveAndValidate()) {
                              // "ticketName": "string",
                              // "ticketType": "MONTH",
                              // "maxMonth": 0,
                              // "unitPrice": 0
                              print(
                                  "ticketName : ${_formKey.currentState!.fields['ticketName']!.value}");
                              print(
                                  "ticketType : ${_formKey.currentState!.fields['ticketType']!.value}");
                              print(
                                  "maxMonth : ${_formKey.currentState!.fields['maxMonth']!.value}");
                              print(
                                  "unitPrice : ${_formKey.currentState!.fields['unitPrice']!.value}");
                              //_setCustomer(request);
                            }
                          },
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
