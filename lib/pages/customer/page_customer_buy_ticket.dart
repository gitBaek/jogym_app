import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_no_contents.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_pt_ticket.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_season_ticket.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/buy/buy_pt_ticket_request.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_item.dart';
import 'package:jo_gym_app/model/season/season_ticket_item.dart';
import 'package:jo_gym_app/repository/buy/repo_buy_pt_ticket.dart';
import 'package:jo_gym_app/repository/pt/repo_pt_ticket.dart';
import 'package:jo_gym_app/repository/season/repo_season_ticket.dart';

class PageCustomerBuyTicket extends StatefulWidget {
  const PageCustomerBuyTicket({super.key, required this.customerId});

  final int customerId;

  @override
  State<PageCustomerBuyTicket> createState() => _PageCustomerBuyTicketState();
}

// "address": "string",
// "dateBirth": "string",
// "gender": "MALE",
// "name": "string",
// "phoneNumber": "string"

class _PageCustomerBuyTicketState extends State<PageCustomerBuyTicket> {
  bool _isBtnSwitch = true;
  final _scrollController = ScrollController();

  List<PtTicketItem> _ptTicketList = [];
  int _ptTotalItemCount = 0;
  int _ptTotalPage = 1;
  int _ptCurrentPage = 1;

  List<SeasonTicketItem> _seasonTicketList = [];
  int _seasonTotalItemCount = 0;
  int _seasonTotalPage = 1;
  int _seasonCurrentPage = 1;



  Future<void> _doBuyPtTicket(BuyPtTicketRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    print("고객 아이디 : ${request.customerId}");
    print("PT 아이디 : ${request.ptTicketId}");

    await RepoBuyPtTicket().doBuyPtTicket(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '구매 완료',
        subTitle: '정기권 구매가 완료되었습니다.',
      ).call();

    }).catchError((err) {
      print(err.toString());
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '구매 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  Future<void> _loadSeasonItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _seasonTicketList = [];
      _seasonTotalItemCount = 0;
      _seasonTotalPage = 1;
      _seasonCurrentPage = 1;
    }

    if (_seasonCurrentPage <= _seasonTotalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoSeasonTicket()
          .getList(page: _seasonCurrentPage)
          .then((res) => {
                BotToast.closeAllLoading(),
                setState(() {
                  _seasonTotalItemCount = res.totalItemCount;
                  _seasonTotalPage = res.totalPage;
                  _seasonTicketList = [
                    ..._seasonTicketList,
                    ...?res.list
                  ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
                  _seasonCurrentPage++;
                })
              })
          .catchError((err) => {
                BotToast.closeAllLoading(),
                print(err),
              });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  Future<void> _loadPtItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _ptTicketList = [];
      _ptTotalItemCount = 0;
      _ptTotalPage = 1;
      _ptCurrentPage = 1;
    }

    if (_ptCurrentPage <= _ptTotalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoPtTicket()
          .getList(page: _ptCurrentPage)
          .then((res) => {
                BotToast.closeAllLoading(),
                setState(() {
                  _ptTotalItemCount = res.totalItemCount;
                  _ptTotalPage = res.totalPage;
                  _ptTicketList = [
                    ..._ptTicketList,
                    ...?res.list
                  ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
                  _ptCurrentPage++;
                })
              })
          .catchError((err) => {
                BotToast.closeAllLoading(),
                print(err),
              });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _isBtnSwitch ? _loadSeasonItems() : null;
      }
    });
    _isBtnSwitch ? _loadSeasonItems() : null;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "회원권 구매"),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _isBtnSwitch
              ? _loadSeasonItems(reFresh: true)
              : _loadPtItems(reFresh: true);
        },
        child: const Icon(Icons.refresh),
      ),
      body: Column(
        children: [
          const ComponentMarginVertical(enumSize: EnumSize.mid),
          Container(
            padding: bodyPaddingLeftRightAndVerticalHalf,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (_isBtnSwitch)
                      ? ComponentTextBtn(
                          '정기권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('정기권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                            _loadSeasonItems(reFresh: true);
                          });
                        }),
                ),
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (!_isBtnSwitch)
                      ? ComponentTextBtn(
                          'PT권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('PT권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                            _loadPtItems(reFresh: true);
                          });
                        }),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              controller: _scrollController,
              child: _isBtnSwitch ? _buildSeasonBody() : _buildPtBody(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSeasonBody() {
    if (_seasonTotalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _seasonTicketList.length,
              itemBuilder: (_, index) => ComponentSeasonTicket(
                    number: _seasonTicketList[index].id,
                    seasonTicketName: _seasonTicketList[index].ticketName,
                    seasonTicketType: _seasonTicketList[index].ticketType,
                    month: _seasonTicketList[index].maxMonth.toString(),
                    unitPrice: _seasonTicketList[index].unitPrice.toString(),
                    totalPrice: _seasonTicketList[index].totalPrice.toString(),
                    voidCallback: () {
                      _ShowDialog(
                        titleText: "정기권 구매",
                        titleColor: Colors.black,
                        contentText: "정말 구매하시겠습니까?",
                        contentColor: Colors.black,
                        callbackOk: () {

                        },
                        callbackCancel: () {
                          Navigator.pop(context);
                        },
                      );
                    },
                  )),
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents(
            icon: Icons.history,
            msg: '등록된 정기권이 없습니다.',
          ),
          onTap: () => _loadSeasonItems(reFresh: true),
        ),
      );
    }
  }

  Widget _buildPtBody() {
    if (_ptTotalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _ptTicketList.length,
            itemBuilder: (_, index) => ComponentPtTicket(
              number: _ptTicketList[index].id,
              ptTicketName: _ptTicketList[index].ticketName,
              maxCount: _ptTicketList[index].maxCount.toString(),
              unitPrice: _ptTicketList[index].unitPrice.toString(),
              totalPrice: _ptTicketList[index].totalPrice.toString(),
              voidCallback: () {
                // PT권 구매
                _ShowDialog(
                  titleText: "PT권 구매",
                  titleColor: Colors.black,
                  contentText: "정말 구매하시겠습니까?",
                  contentColor: Colors.black,
                  callbackOk: () {
                    BuyPtTicketRequest request = BuyPtTicketRequest(widget.customerId,_ptTicketList[index].id);
                    _doBuyPtTicket(request);
                  },
                  callbackCancel: () {
                    Navigator.pop(context);
                  },
                );
              },
            ),
          ),
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents(
            icon: Icons.history,
            msg: '등록된 PT권이 없습니다.',
          ),
          onTap: () => _loadPtItems(reFresh: true),
        ),
      );
    }
  }

  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  }) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }
}
