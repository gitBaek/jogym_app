import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/component_divider.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/calculate/calculate_update_request.dart';
import 'package:jo_gym_app/repository/calculate/repo_calculate.dart';
import 'package:jo_gym_app/styles/style_form_decoration_full_going.dart';

class PageCalculateDetail extends StatefulWidget {
  const PageCalculateDetail({super.key,
    required this.calculateId,
  });

  final int calculateId;

  @override
  State<PageCalculateDetail> createState() => _PageCalculateState();
}

class _PageCalculateState extends State<PageCalculateDetail> {
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textControllerOfDateCreateYear = TextEditingController();
  final TextEditingController _textControllerOfDateCreateMonth = TextEditingController();
  final TextEditingController _textControllerOfFeesRate = TextEditingController();
  final TextEditingController _textControllerOfMinusPrice = TextEditingController();
  final TextEditingController _textControllerOfCalculatePrice = TextEditingController();
  final TextEditingController _textControllerOfTotalPrice = TextEditingController();
  final TextEditingController _textControllerOfCalculateStatus = TextEditingController();


  bool _isFixEnable = false;

  // ING, COMPLETE


  // "dateCreateYear": 2022,
  // "dateCreateMonth": 7,
  // "feesRate": 0.12,
  // "totalPrice": 0,
  // "minusPrice": 0,
  // "calculatePrice": 0,
  // "calculateStatus": "ING"


  Future<void> _getCalculate(int calculateId) async { // 상세
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCalculate().getData(calculateId: calculateId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _textControllerOfDateCreateYear.text = res.data.dateCreateYear.toString(); // int, double은 toString()을 붙혀줘야만 함.
        _textControllerOfDateCreateMonth.text = res.data.dateCreateMonth.toString();
        _textControllerOfTotalPrice.text = res.data.totalPrice.toString();
        _textControllerOfCalculatePrice.text = res.data.calculatePrice.toString();
        _textControllerOfFeesRate.text = res.data.feesRate.toString();
        _textControllerOfMinusPrice.text = res.data.minusPrice.toString();
        switch(res.data.calculateStatus){
          case "ING" :
            _textControllerOfCalculateStatus.text = "정산중";
            break;
          case "COMPLETE" :
            _textControllerOfCalculateStatus.text = "정산완료";
            break;
        }
      });

      ComponentNotification(
        success: true,
        title: '정산내역 불러오기 완료',
        subTitle: '정산내역 불러오기가 완료되었습니다.',
      ).call();
    }).catchError((err) {
      print(err.toString());
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '정산내역 불러오기 실패',
        subTitle: '내용을 확인 해주세요.',
      ).call();
    });
  }

  Future<void> _putCalculate(int calculateId, CalculateUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCalculate()
        .putData(calculateId: calculateId, request: request)
        .then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _isFixEnable = false;
      });
      ComponentNotification(
        success: true,
        title: '정산내역 저장 완료',
        subTitle: '정산내역 저장이 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '정산내역 저장 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }


  @override
  void initState() {
    // TODO: implement intiState
    super.initState();
    _getCalculate(widget.calculateId);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '정산내역 상세 리스트'),
      body: SingleChildScrollView(
        padding: bodyPaddingLeftRight,
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ComponentMarginVertical(enumSize: EnumSize.mid),
              FormBuilderTextField(
                name: 'dateCreateYear',
                controller: _textControllerOfDateCreateYear,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('등록년'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'dateCreateMonth',
                controller: _textControllerOfDateCreateMonth,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('등록월'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'totalPrice',
                controller: _textControllerOfTotalPrice,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('총 매출금'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'feesRate',
                controller: _textControllerOfFeesRate,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('수수료율'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'minusPrice',
                controller: _textControllerOfMinusPrice,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('공제금'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'calculatePrice',
                controller: _textControllerOfCalculatePrice,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('정산금'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'calculateStatus',
                controller: _textControllerOfCalculateStatus,
                readOnly: true,
                decoration: StyleFormDecorationOfInputText()
                    .getInputDecoration('정산상태'),
                keyboardType: TextInputType.text,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(
                      errorText: formErrorRequired),
                ]),
              ),

          ],
          ),
        ),
      ),
    );
  }
}
// calculateDetail 수정 필요