class MemberJoinRequest {
  String username;
  String password;
  String passwordRe;
  String name;
  String phoneNumber;

  MemberJoinRequest(this.username, this.password, this.passwordRe, this.name, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['password'] = this.password;
    data['passwordRe'] = this.passwordRe;
    data['name'] = this.name;
    data['phoneNumber'] = this.phoneNumber;

    return data;
  }
}