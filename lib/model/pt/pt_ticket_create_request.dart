class PtTicketCreateRequest {
  String ticketName;
  int maxCount;
  num unitPrice;


  PtTicketCreateRequest(this.ticketName, this.maxCount, this.unitPrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['ticketName'] = ticketName;
    data['maxCount'] = maxCount;
    data['unitPrice'] = unitPrice;

    return data;
  }
}

// "ticketName": "두번째 PT",
// "maxCount": 10,
// "unitPrice": 500000
